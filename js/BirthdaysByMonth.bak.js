/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


CRM.$(function ($) {
     $('[name=field_contactSubType').crmEntityRef({
        entity: 'contact_type',
        select: {minimumInputLength:0},
        api: {
            params: {
                //parents: 34 // dc id
            }},
        create: false,
    });
    
    $('#bGetBirthdays').click(function () {
        var month = $('#thisMonth').val(); // get the month we are looking for
        $('#resultTableBody').empty();
        var year = new Date().getFullYear();
        var age = "";
        var subtype =  $('#thisSubType').val();
        console.log('subtype: ' + subtype);
        console.log("year: " + year);
        CRM.api3('BirthdaysByMonth', 'get', {
            "sequential": 1,
            "month": month,
            "contact_sub_type" : subtype
        }).done(function (data) {
            $.each(data.values, function (key, value) {
                //console.log(value);
                var fullAddress = value.street_address + ", " + value.city + ", " + value.name + " " + value.postal_code;
                age = year - value.birth_year;
                //console.log('Full Address: ' + fullAddress);
                $('#resultTable > tbody:last').append(
                        "<tr id='rowid" + value.id + "'>" +
                        '<td>' +
                        value.display_name +
                        '</td>' +
                        '<td>' +
                        age +
                        '</td>' +
                        '<td>' +
                        value.birth_date +
                        '</td>' +
                        '<td>' +
                        fullAddress +
                        '</td>' +
                        '</tr>'
                        );
            });
        });
    });


});