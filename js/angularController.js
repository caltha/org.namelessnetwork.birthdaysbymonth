/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var childrenArray = [
        {'name': 'first', 'age': '2', 'birthday': '00-00-0000', 'address': 'none'},
        {'name': 'second', 'age': '1', 'birthday': '00-00-0000', 'address': 'none'},
        {'name': 'third', 'age': '5', 'birthday': '00-00-0000', 'address': 'none'}
];

var myApp = angular.module('birthdaysByMonthApp', [])
    .config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
      }]);
  
myApp.controller('birthdaysCtrl',function ($scope) {
    $scope.people = [];
    $scope.predicate = 'birth_day';
    $scope.reverse = false;
    
    $scope.sort = function(predicate) {
        if ($scope.predicate == predicate) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.reverse = false;
        }
        $scope.predicate = predicate;
    }
    
    $scope.getBirthdays = function() {
        var month = $scope.month; //cj('#thisMonth').val(); // get the month we are looking for
        var year = new Date().getFullYear();
        var group = cj('#thisGroup').val();
        var subtype = cj('#thisSubType').val(); //$scope.subtype;
        //console.log('subtype: ' + subtype);
        //console.log("year: " + year);

        var people = [];

        CRM.api3('BirthdaysByMonth', 'get', {
            "sequential": 1,
            "month": month,
            "group": group,
            "contact_sub_type" : subtype
        }).done(function (data) {
            angular.forEach(data.values, function (key, value) {
                var person = {};

                //console.log(value);

                person['cid'] = key.id;
                person['name'] = key.display_name;
                person['age'] = year - key.birth_year;
                person['birthday'] = key.birth_date;
                person['birth_day'] = parseInt(key.birth_day);
                person['birthday_formatted'] = key.birthday_formatted;
                person['address'] = key.street_address + ", " + key.postal_code + " " + key.city;

                people.push(person);

                //console.log('Full Address: ' + fullAddress);
            });
            
            $scope.people = people;
            $scope.$apply();
        });
    };
});