
{*{crmScript ext=org.namelessnetwork.smallgrouptracking url=https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.js}*}
{crmScript ext=org.namelessnetwork.birthdaysbymonth file=js/angular/angular.min.js}
{crmScript ext=org.namelessnetwork.birthdaysbymonth file=js/BirthdaysByMonth.js}
{crmScript ext=org.namelessnetwork.birthdaysbymonth file=js/angularController.js}
{crmStyle ext=org.namelessnetwork.birthdaysbymonth file=css/BirthdaysByMonth.css}

<div ng-app="birthdaysByMonthApp">
    <div ng-controller="birthdaysCtrl">
        <div class="searchFields">
        <div class="queryField">
            <div class="calloutRow">{ts}Wybierz miesiąc{/ts}</div>
            <select class="filterField" id="thisMonth" ng-model="month">
                <option value="1">{ts}Styczeń{/ts}</option>
                <option value="2">{ts}Luty{/ts}</option>
                <option value="3">{ts}Marzec{/ts}</option>
                <option value="4">{ts}Kwiecień{/ts}</option>
                <option value="5">{ts}Maj{/ts}</option>
                <option value="6">{ts}Czerwiec{/ts}</option>
                <option value="7">{ts}Lipiec{/ts}</option>
                <option value="8">{ts}Sierpień{/ts}</option>
                <option value="9">{ts}Wrzesień{/ts}</option>
                <option value="10">{ts}Październik{/ts}</option>
                <option value="11">{ts}Listopad{/ts}</option>
                <option value="12">{ts}Grudzień{/ts}</option>
            </select>
        </div>
        <div id='divSelectGroup' class="queryField"> 
            <div class="calloutRow">{ts}Filtr: Grupa (opcjonalny){/ts}</div>
            <input class="filterField" id="thisGroup" name="field_group" placeholder="{ts}- select -{/ts}"/>
        </div>
        <div id='divSelectDC' class="queryField"> 
            <div class="calloutRow">Filtr: Rodzaj kontaktu (opcjonalny)</div>
            <input id="thisSubType" class="filterField" name="field_contactSubType" placeholder="{ts}- select -{/ts}"/>
        </div>
        <div class="queryField">
             <div class="calloutRow">{ts}szukaj w wynikach{/ts}</div>
            <input class="filterField" placeholder="Search" ng-model="searchText">
        </div>
        <button id='bGetBirthdays' class='button' type='button' ng-click='getBirthdays()'>
            {ts}Szukaj urodzin{/ts}
        </button>
        </div>
        <table id="resultTable" class="selector row-highlight">
            <thead>
                <tr>
                    <th><a ng-click="sort('name')">{ts}Kontakt{/ts}</a></th>
                    <th><a ng-click="sort('age')">{ts}Wiek{/ts}</a></th>
                    <th><a ng-click="sort('birth_day')">{ts}Urodziny (sort. po dniu){/ts}</a></th>
                    <th>{ts}Adres{/ts}</th>
                </tr>
            </thead>
            <tbody id="resultTableBody">
                <tr id="resultRow" ng-repeat="person in people | filter:searchText | orderBy:predicate:reverse">

                    <td id="name"><a href="{crmURL p="civicrm/contact/view" q="reset=1&cid=[[person.cid]]"}">[[person.name]]</a></td>
                    <td id="age">[[person.age]]</td>
                    <td id="birthday">[[person.birthday_formatted]]</td>
                    <td id="address">[[person.address]]</td>

                </tr>
            </tbody>
        </table>
    </div>
</div>