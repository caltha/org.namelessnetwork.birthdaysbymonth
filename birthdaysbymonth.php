<?php

require_once 'birthdaysbymonth.civix.php';

/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function birthdaysbymonth_civicrm_config(&$config) {
  _birthdaysbymonth_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function birthdaysbymonth_civicrm_install() {
  return _birthdaysbymonth_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function birthdaysbymonth_civicrm_enable() {
  return _birthdaysbymonth_civix_civicrm_enable();
}

// /**
//  * Implements hook_civicrm_postInstall().
//  *
//  * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
//  */
// function birthdaysbymonth_civicrm_postInstall() {
//   _birthdaysbymonth_civix_civicrm_postInstall();
// }

// /**
//  * Implements hook_civicrm_entityTypes().
//  *
//  * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
//  */
// function birthdaysbymonth_civicrm_entityTypes(&$entityTypes) {
//   _birthdaysbymonth_civix_civicrm_entityTypes($entityTypes);
// }
